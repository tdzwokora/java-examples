public class Main {
    private String greeting = "Hie";

    public class InnerClass {
        public int repeat = 3;

        public void go() {
            for (int i = 0; i < repeat; i++) {
                System.out.println(greeting);
            }
        }
    }

    public  void callInner(){
        InnerClass inner = new InnerClass();
        inner.go();
    }

    public static void main(String[] args) {
        Main main = new Main();
        InnerClass inner = new Main().new InnerClass();
        inner.go();
    }
}
