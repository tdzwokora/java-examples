package com.tinashe;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {


    public static void main(String[] args) {

        //reduction



        //parallel vs sequential unpredictable vs predictable
        //List.of(1,2,3,4,5,6).stream().forEach(s-> System.out.println(s));
//sequential n parrallel stream
//PERFORMING A PARALLEL DECOMPOSITION
        //A parallel
        //decomposition is the process of taking a task, breaking it up into smaller
        //pieces that can be performed concurrently, and then reassembling the
        //results. The more concurrent a decomposition, the greater the performance
        //improvement of using parallel streams.

        /*long start = System.currentTimeMillis();
        List.of(1,2,3,4,5)
                //.stream()
                .parallelStream()
                .map(w -> doWork(w))
                //.forEach(s -> System.out.print(s + " "));
                .forEachOrdered(s -> System.out.print(s + " "));
        System.out.println();
        var timeTaken = (System.currentTimeMillis()-start)/1000;
        System.out.println("Time: "+timeTaken+" seconds");*/

        //As you might expect, the results are ordered and predictable because we
        //are using a serial stream. It also took around 25 seconds to process all five
        //results, one at a time. What happens if we use a parallel stream, though?

        //parallel With a parallel stream, the map() and forEach() operations are applied
        //concurrently. The following is sample output:

        //use a parallel stream and parallel reduction:
        /*Stream<String> ohMy =
                Stream.of("lions","tigers","bears").parallel();
        ConcurrentMap<Integer, String> map = ohMy
                .collect(Collectors.toConcurrentMap(String::length,
                        k -> k,
                        (s1, s2) -> s1 + "," + s2));
        System.out.println(map); // {5=lions,bears, 6=tigers}
        System.out.println(map.getClass()); //        java.util.concurrent.ConcurrentHashMap*/

        //we can rewrite our groupingBy()  to use
        //a parallel stream and parallel reduction.
        /*var ohMy = Stream.of("lions","tigers","bears").parallel();
        ConcurrentMap<Integer, List<String>> map = ohMy.collect(
                Collectors.groupingByConcurrent(String::length));
        System.out.println(map); // {5=[lions, bears], 6=[tigers]}*/


   /*Suppose that we are making a sign to put outside each animal's exhibit.
We have two sizes of signs. One can accommodate names with five or
fewer characters. The other is needed for longer names. We can partition
the list according to which sign we need.*/
   /*
        var ohMy = Stream.of("lions", "tigers", "bears");
        Map<Boolean, List<String>> map = ohMy.collect(
                Collectors.partitioningBy(s -> s.length() <= 5));
        System.out.println(map);// {false=[tigers], true=[lions,bears]}*/



    }
    public static int doWork(int input) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {}
        return input;
    }

}

