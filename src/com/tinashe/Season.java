package com.tinashe;

public enum Season {
    FALL("Medium"),
    SPRING("Medium"),
    SUMMER("High"){
        @Override public String getHours() { return "9am-7pm";}
    },
    WINTER("Low"){
        @Override  public String getHours() { return "10am-3pm"; }
    };

    private final String expectedVisitors;

    Season(String expectedVisitors) {
        this.expectedVisitors = expectedVisitors;
    }

    public void printExpectedVisitors() {
        System.out.println(expectedVisitors);
    }
    public  String getHours(){
        return "9am- 5pm";
    };


}
