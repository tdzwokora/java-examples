package com.tinashe.streamsAndLambda;

public class Employee {
    private int empid;
    private String name;
    private double salary;
    private String gender;

    public Employee() {
        super();
    }

    public Employee(int empid, String name,
                    double salary, String gender) {
        super();
        this.empid = empid;
        this.name = name;
        this.salary = salary;
        this.gender = gender;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Employee [empid=" + empid + ", name="
                + name + ", salary=" + salary + " gender=" + gender+ "]";
    }
}
