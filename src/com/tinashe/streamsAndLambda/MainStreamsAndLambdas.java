package com.tinashe.streamsAndLambda;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.*;
import java.util.stream.Stream;

public class MainStreamsAndLambdas {

    public static void main(String[] args) {
        Supplier<LocalDate> s1 = LocalDate::now;
        Supplier<LocalDate> s2 = () -> LocalDate.now();
        LocalDate d1 = s1.get();
        LocalDate d2 = s2.get();
        // System.out.println(d1 + "\n" + d2);

        Supplier<StringBuilder> s3 = StringBuilder::new;
        Supplier<StringBuilder> s4 = () -> new StringBuilder();
        //System.out.println(s3.get() +"\n" + s4.get());

        Supplier<ArrayList<String>> s5 = ArrayList<String>::new;
        ArrayList<String> arrayList = s5.get();
        //System.out.println(arrayList);

        Consumer<String> consumer1 = System.out::println;
        Consumer<String> consumer2 = x -> System.out.println(x);
        //consumer1.accept("Tinashe");
        //consumer2.accept("Tinashe");

        var map = new HashMap<String, Integer>();
        BiConsumer<String, Integer> biConsumer1 = map::put;
        BiConsumer<String, Integer> biConsumer2 = (k, v) -> map.put(k, v);
        biConsumer1.accept("Chicken", 7);
        biConsumer2.accept("Chick", 1);
        //System.out.println(map);

        Predicate<String> predicate1 = String::isEmpty;
        Predicate<String> predicate2 = x -> x.isEmpty();
        //System.out.println(predicate1.test("") + "\n" + predicate2.test(""));

        BiPredicate<String, String> biPredicate1 = String::startsWith;
        BiPredicate<String, String> biPredicate2 = (string, prefix) -> string.startsWith(prefix);
        //System.out.println(biPredicate1.test("chicken", "chick"));
        //System.out.println(biPredicate2.test("chicken", "chick"));

        Function<String, Integer> function1 = String::length;
        Function<String, Integer> function2 = x -> x.length();
        //System.out.println(function1.apply("chuck"));
        //System.out.println(function2.apply("chuck"));

        BiFunction<String, String, String> biFunction1 = String::concat;
        BiFunction<String, String, String> biFunction2 = (string, toAdd) -> string.concat(toAdd);
        //System.out.println(biFunction1.apply("baby ", "chick"));
        //System.out.println(biFunction2.apply("baby ", "chick"));

        UnaryOperator<String> unaryOperator1 = String::toUpperCase;
        UnaryOperator<String> unaryOperator2 = x->x.toUpperCase();
        //System.out.println(unaryOperator1.apply("Chirp"));
        //System.out.println(unaryOperator2.apply("Chirp"));

        BinaryOperator<String> binaryOperator1 = String::concat;
        BinaryOperator<String> binaryOperator2 = (string, toAdd) -> string.concat(toAdd);
        //System.out.println(binaryOperator1.apply("baby ", "chick"));
        //System.out.println(binaryOperator2.apply("baby ", "chick"));

        Stream<String> stream = Stream.of("test","tesw","tesd");
        System.out.println(stream.count());


    }
}

