package com.tinashe.streamsAndLambda;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Reduction {
    public static void main(String[] args) {
        var array = new String[] { "w", "o", "l", "f" };
        var result = "";
        for (var s: array) result = result + s;
        //System.out.println(result);


        //reduction using streams
        Stream<String> stream = Stream.of("w", "o", "l", "f");
        String word = stream.reduce("", (s, c) -> s + c);
        //System.out.println(word);

        Stream<String> stream1 = Stream.of("w", "o", "l", "f");
        String word1 = stream1.reduce("", String::concat);
        //System.out.println(word1);

        //Can you write a reduction to multiply all of the Integer objects in a stream?
        Stream<Integer> integerStream = Stream.of(3,5,6);
        //int output = integerStream.reduce(1,(a,b)->a*b); //accumulator

        //System.out.println(output);

        //The collect() method is a special type of reduction called a mutable reduction
        //It is more efficient than a regular reduction because we use the same mutable object while accumulating.
        Stream<String> stream2 = Stream.of("w", "o", "l", "f");
        StringBuilder wordy = stream2.collect(
                StringBuilder::new,               //Supplier
                StringBuilder::append,              //BiConsumer accumulator
                StringBuilder::append               //BiConsumer combiner
        );
        //StringBuilder wordy = stream2.collect(Collectors.toCollection())
        //System.out.println(wordy);

        List<Integer> numbers = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        //Integer sum = numbers.stream().reduce(0, (n1, n2) -> n1 + n2); // must be 36
        //Integer sum = numbers.stream().reduce(0, Integer::sum);
        int sum = numbers.parallelStream().reduce(0, Integer::sum);
        System.out.print("Sum of all integers in the stream : " + sum);


    }
}
