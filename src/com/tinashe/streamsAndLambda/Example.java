package com.tinashe.streamsAndLambda;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static com.tinashe.Main.doWork;
import static java.util.stream.Collectors.*;

public class Example {



    public static void main(String[] args) {
        System.out.println("=========================== Streams and Lambdas=================================");

        Supplier<int[]> numbers = () -> {
            int[] number = {1, 2, 3, 4, 5, 6};
            return number;
        };

        //original array
        System.out.println("===================================== Original array==============================");
        for (int n : numbers.get()) {
            System.out.print(n);
        }

      /*  System.out.println();
        //print using sequential
        System.out.println("============================ Sequential stream =========================================");
        Arrays.stream(numbers.get()).forEach(System.out::print);
        System.out.println();
*/

       /* //print using parallel
        System.out.println("============================ Parallel stream =========================================");
        Arrays.stream(numbers.get()).parallel().forEach(System.out::print);
        System.out.println();*/

        //The Stream interface has two methods for reduction:
        //collect()
        //reduce()
        //use a for loop to reduce an array of numbers to their sum:
        /*System.out.println("============================== Reduction using a loop ===================================");
        int sum = 0;
        for (int n : numbers.get()) {
            sum += n;
        }
        System.out.println("Sum is " + sum);*/


        //benefits, such as easier parallelization and improved readability.
        System.out.println("============================== Reduction using a Streams ===================================");
        int identity = 0;
        IntBinaryOperator accumulator = (sum1, n) -> sum1 + n;
        int total = Arrays.stream(numbers.get()).reduce(identity, accumulator);
        System.out.println("Sum is " + total);



        /*System.out.println("=================== the sum of the length of all strings of a stream ===============");
        BiFunction<Integer, String, Integer> accumulator1 = (accumInt, str) -> accumInt + str.length();
        BinaryOperator<Integer> combiner = (accumInt1, accumInt2) -> accumInt1 + accumInt2;
        int identity1 = 0;
        int length = Stream.of("Parallel", "streams", "are", "great").reduce(identity1, accumulator1, combiner);
        System.out.println("Sum of lengths of strings is " + length);*/



        /*System.out.println("=================== the sum of the length of all strings of a stream  using map reduce pattern===============");
        ToIntFunction<String> mapper = s -> s.length();
        int length1 = Stream.of("Parallel", "streams", "are", "great")
                .mapToInt(mapper)
                .sum();
        System.out.println("Sum of lengths of strings is " + length1);*/


        //Working with parallel streams
        //Having one function as a mapper and accumulator is more efficient than having separate mapping and reduction functions.

      /*  System.out.println("================================ collectors ============================");
        //Collect
        *//*double avg = Stream.of(1, 2, 3).collect(averagingInt(i -> i * 2)); // 4.0
        System.out.println(avg);*//*


        *//*long count = Stream.of(1, 2, 3).collect(counting()); // 3
        System.out.println(count);*//*

        *//*Stream.of(1, 2, 3).collect(maxBy(Comparator.naturalOrder())).ifPresent(System.out::println); // 3*//*


        Integer sum2 = Stream.of(1, 2, 3).collect(summingInt(i -> i)); // 6
        System.out.println(sum2);*/



       /* //group by tens naturally
        System.out.println("============================= Group by tens naturally=============================");
        List<Integer> stream =Arrays.asList(2,34,54,23,33,20,59,11,19,37);
        Map<Integer, List<Integer>> map1 = new HashMap<>();

        for(Integer i : stream) {
            int key = i/10 * 10;
            List<Integer> list = map1.get(key);

            if(list == null) {
                list = new ArrayList<>();
                map1.put(key, list);
            }
            list.add(i);
        }
        System.out.println(map1);*/



       /* System.out.println("===================================== Grouping by tens using streams ===================================");
        Function<Integer, Integer> classifier = i -> i / 10 * 10;
        Collector<Integer, ?, Map<Integer, List<Integer>>> collector = groupingBy(classifier);
        Map<Integer, List<Integer>> map = Stream.of(2, 34, 54, 23, 33, 20, 59, 11, 19, 37)
                .collect(collector);
        System.out.println(map);
*/



        /*System.out.println("============================= count grouped by tens=======================================");
        Map<Integer, Long> map3 =
                Stream.of(2, 34, 54, 23, 33, 20, 59, 11, 19, 37)
                        .collect(
                                groupingBy(i -> i/10 * 10,
                                        counting()
                                )
                        );

        System.out.println(map3);*/


        /*System.out.println("============================ classify into 2nd level grouping even n odd=================== ");
        Function<Integer, String> evenAndOddFunction = i -> i % 2 == 0 ? "EVEN" : "ODD";
        Function<Integer, Integer> tensFunction = i -> i / 10 * 10;
        Map<Integer, Map<String, List<Integer>>> map2 =
                Stream.of(2,34,54,23,33,20,59,11,19,37)
                        .collect(groupingBy(tensFunction,
                                groupingBy(evenAndOddFunction)
                                )
                        );
        System.out.println(map2);*/


       /* System.out.println("======================================= partitioningBy()=================================");
        Predicate<Integer> lessThan50Predicate = i -> i < 50;
        Map<Boolean, List<Integer>> map4 =
                Stream.of(45, 9, 65, 77,12, 12, 89, 31)
                        .collect(partitioningBy(lessThan50Predicate));
        System.out.println(map4);*/




        /*System.out.println("================================remove duplicates=====================================");
        Predicate<Integer> lessThan50Predicate = i -> i < 50;
        Map<Boolean, Set<Integer>> map5=
                Stream.of(45, 9, 65, 77, 12, 89, 31, 12)
                        .collect(
                                partitioningBy(lessThan50Predicate,
                                        toSet()
                                )
                        );
        System.out.println(map5);*/



        //====================================================================================================================================================

        //Parallel streams split the stream into multiple parts. Each part is processed by a different thread at the same time (in parallel).
        //by default, the number of threads available to process parallel streams equals the number of available cores in your machine's processor (CPU).

        /*Stream<String> parallelStream = Stream.of("a","b","c","d","e").parallel();

        //parallel stream from a collection
        List<String> list = Arrays.asList("a","b","c");
        Stream<String> parStream = list.parallelStream();

        //You can turn a parallel stream into a sequential one
        System.out.println("================== Sequential Stream=============================");
        Stream.of("a","b","c","d","e").forEach(System.out::print);
        System.out.println();*/

        /*System.out.println("================== Parallel Stream=============================");
        Stream.of("a","b","c","d","e").parallel().forEach(System.out::print);
        System.out.println();*/

       /* System.out.println("======================= Parallel turned into sequential============================");
        Stream.of("a","b","c","d","e").parallel().filter(x->x.length()>0).sequential().forEach(System.out::print);
        System.out.println();*/

        /*System.out.println("===================Check if a stream is parallel with isParallel()====================");
        Stream<String> parallelStream = Stream.of("a","b","c","d","e").parallel();
        System.out.println(parallelStream.isParallel());*/



        ///
        /*long start = System.nanoTime();
        String first = Stream.of("a","b","c","d","e")
                .parallel().findFirst().get();
        long duration = (System.nanoTime() - start) / 1000000;
        System.out.println(first + " found in " + duration + " milliseconds");*/

        /*long start = System.nanoTime();
        String any = Stream.of("a","b","c","d","e")
                .findAny().get();
        long duration = (System.nanoTime() - start) / 1000000;
        System.out.println(
                any + " found in " + duration + " milliseconds");*/



        //compare time of execution parallel taking longer
        /*double start = System.nanoTime();
        Stream.of("b","d","a","c","e")
                .sorted()
                .filter(s -> {
                    System.out.println("Filter:" + s);
                    return !"d".equals(s);
                })
                //.parallel()                 //comment out parallel
                .map(s -> {
                    System.out.println("Map:" + s);
                    return s += s;
                })
                .forEach(System.out::println);
        double duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println(duration + " milliseconds");
*/

        //But if we have an independent or stateless operation, and order doesn't matter, such as
        // with counting the number of odd numbers in a large range, the parallel version will perform better:
       /* double start1 = System.nanoTime();
        long c = IntStream.rangeClosed(0, 1_000_000_000)
                //.parallel()
                .filter(i -> i % 2 == 0)
                .count();
        double duration1 = (System.nanoTime() - start1) / 1_000_000;
        System.out.println("Got " + c + " in " + duration1 + " milliseconds");*/

        /*
        long start2 = System.currentTimeMillis();
        List.of(1,2,3,4,5)
                //.stream()
                .parallelStream()
                .map(w -> doWork(w))
                //.forEach(s -> System.out.print(s + " "));
                .forEachOrdered(s -> System.out.print(s + " "));
        System.out.println();
        var timeTaken = (System.currentTimeMillis()-start2)/1000;
        System.out.println("Time: "+timeTaken+" seconds");*/

        /*

        //In summary, parallel streams don't always perform better than sequential streams when it comes to stateful operations,
        // but they usually perform better when ordering is not an issue and operations are independent and stateless.
        //
        //This, the fact that parallel streams process results independently, and the idea that the order cannot be
        // guaranteed are the most important things you need to know.


        //Reducing Parallel Streams
        System.out.println("=====================================Reducing Parallel Streams===================================");

        System.out.println("===================================== 10 factorial using sequential==========================");
        class Total {
            public long total = 1;
            public void multiply(long n) { total *= n; }
        }

        Total t = new Total();
        LongStream.rangeClosed(1, 10)
                //.parallel()
                .forEach(t::multiply);
        System.out.println(t.total);

        //When we execute this snippet of code, it produces the correct result every time (3628800).
        // Reduce guaranteed that the threads would not access the same stream entries simultaneously and throw off the results
        long tot = LongStream.rangeClosed(1, 10)
                .parallel()
                .reduce(1, (a,b) -> a*b);
        System.out.println(tot);
        */
    }
}
