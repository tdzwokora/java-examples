package com.tinashe.streamsAndLambda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
                new Employee(1276, "Panashe",2000.00, "Male"),
                new Employee(7865, "Thomas",1200.00, "Female"),
                new Employee(4975, "Peter",3000.00, "Female"),
                new Employee(4499, "John",1500.00, "Female"),
                new Employee(9937, "Travor",2800.00, "Male"),
                new Employee(5634, "Mark",1100.00, "Female"),
                new Employee(9276, "Luke",3200.00, "Male"),
                new Employee(6852, "James",3400.00, "Male"));

        System.out.println("Original List");
        printList(employees);

        // Using sequential stream
        long start = System.currentTimeMillis();
        List<Employee> sortedItems = employees.stream()
                .sorted(Comparator
                        .comparing(Employee::getName))
                .collect(Collectors.toList());
        long end = System.currentTimeMillis();

        System.out.println("sorted using sequential stream");
        printList(sortedItems);
        System.out.println("Total the time taken process :"
                + (end - start) + " milisec.");

        // Using parallel stream
        start = System.currentTimeMillis();
        List<Employee> anotherSortedItems = employees
                .parallelStream().sorted(Comparator
                        .comparing(Employee::getName))
                .collect(Collectors.toList());
        end = System.currentTimeMillis();

        System.out.println("sorted using parallel stream");
        printList(anotherSortedItems);
        System.out.println("Total the time taken process :"
                + (end - start) + " milisec.");


        double totsal=employees.parallelStream()
                .map(e->e.getSalary())
                .reduce(0.00,(a1,a2)->a1+a2);
        System.out.println("Total Salary expense: "+totsal);
        System.out.println("Employee with highest salary");
        Optional<Employee> maxSal=employees.parallelStream()
                .reduce((Employee e1, Employee e2)->
                        e1.getSalary()<e2.getSalary()?e2:e1);
        if(maxSal.isPresent())
            System.out.println(maxSal.get().toString());

        System.out.println("=========================== Group By Gender parallel stream========");
        ConcurrentMap<String, List<Employee>> byGender =
                employees
                        .parallelStream()
                        .collect(
                                Collectors.groupingByConcurrent(Employee::getGender));
        System.out.println(byGender);
       /* ConcurrentMap<Integer, String> map = ohMy
                .collect(Collectors.toConcurrentMap(String::length,
                        k -> k,
                        (s1, s2) -> s1 + "," + s2));*/
    }

    public static void printList(List<Employee> list) {
        for (Employee e : list)
            System.out.println(e.toString());
    }
}


