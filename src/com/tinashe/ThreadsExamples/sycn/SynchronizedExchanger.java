package com.tinashe.ThreadsExamples.sycn;

public class SynchronizedExchanger {

    public static Object staticObj = null;

    public static synchronized Object getObject() {
        return staticObj;
    }

    //only one thread can execute ths method
    public static synchronized void setObject(Object object) {
        staticObj = object;
    }

    public Object instanceObject = null;

    public synchronized Object getObj() {
        return instanceObject;
    }

    public synchronized void setObj(Object object) {
       // synchronized (SynchronizedExchanger.class){}
        this.instanceObject = object;
    }
}
