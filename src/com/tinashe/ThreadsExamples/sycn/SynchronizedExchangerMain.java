package com.tinashe.ThreadsExamples.sycn;

public class SynchronizedExchangerMain {

    public static void main(String[] args){
        SynchronizedExchanger exchanger = new SynchronizedExchanger();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<1000; i++){
                    exchanger.setObject("" + i);
                }
            }
        });

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<1000; i++){
                    System.out.println(exchanger.getObject());
                }
            }
        });

        thread.start();
        thread1.start();
    }
}
