package com.tinashe.ThreadsExamples;

public class ThreadExample {

    public static class MyThread extends Thread {
        @Override
        public void run() {
            System.out.println("MyThread running");
            System.out.println("MyThread finished");
        }
    }

    public static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("MyRunnable running");
            System.out.println("MyRunnable finished");
        }
    }

    public static void main(String[] args) {
        //Ways to create a thread
        //1.//create a thread & start
        Thread thread = new Thread();
        thread.start();

        //2. sub class of java Thread
        MyThread myThread = new MyThread();
        myThread.start();

        //3. Runnable
        Thread thread1 = new Thread(new MyRunnable());
        thread1.start();

        //4. Anonymous class
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Runnable running");
                System.out.println("Runnable finished");
            }
        };

        Thread thread2 = new Thread(runnable);
        thread2.start();

        //Reference of current executing thread
        Runnable runnable1 = ()->{
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + " running");
        };

        Thread thread3 = new Thread(runnable1, "The Thread 1");
        thread3.start();

        Thread thread4 = new Thread(runnable1, "The Thread 2");
        thread4.start();


    }
}
