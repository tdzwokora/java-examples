package com.tinashe.ThreadsExamples;

public class VmWillContinueToRun {

    public static void main(String[] args){
        Runnable runnable = () -> {
          for (int i=0; i<5; i++) {
              sleep(1000);
              System.out.println("Running");
          }
        };

        Thread thread = new Thread(runnable, "The Thread");
        thread.setDaemon(true); // will cause the VM to exit wen main thread exits
        thread.start();
        //cause the main thread to wait on the other thread
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Main thread has terminated");
    }

    public static void sleep(long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
