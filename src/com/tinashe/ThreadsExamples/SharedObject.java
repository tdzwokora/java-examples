package com.tinashe.ThreadsExamples;

import javax.crypto.spec.PSource;

public class SharedObject {
    public static class MyRunnable implements Runnable{

        public int count;

        @Override
        public void run() {

            for (int i=0; i<1_000_000; i++){
                synchronized (this){
                    this.count++;
                }
            }
            System.out.println(Thread.currentThread().getName()+ " : " + this.count);
        }
    }

    public static void main(String[] args){
        Runnable runnable = new MyRunnable();
        Thread thread = new Thread(runnable, "Thread 1");
        Thread thread1 = new Thread(runnable, "Thread 2");

        thread.start();
        thread1.start();
    }

}
