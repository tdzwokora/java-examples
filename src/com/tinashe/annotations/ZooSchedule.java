package com.tinashe.annotations;

public @interface ZooSchedule {
    String[] hours();
}
