package com.tinashe.annotations;

public @interface ZooAnimal {
    String habitat() default "anyAnimal";
}
