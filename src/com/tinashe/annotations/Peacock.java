package com.tinashe.annotations;

@ZooAnimal public class Peacock extends Bird {
    @ZooSchedule(hours = {"4am","5pm"}) void cleanPeacocksPen(){
        System.out.println("Time to sweep up!");
    }
}
