package com.tinashe.annotations;

@ZooAnimal public class Lion extends Mammal {
    @ZooSchedule(hours={"9am","5pm","10pm"}) void feedLions() {
        System.out.print("Time to feed the lions!");
    }
}
