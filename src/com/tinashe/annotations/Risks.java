package com.tinashe.annotations;

public @interface Risks {
    Risk[] value();
}
