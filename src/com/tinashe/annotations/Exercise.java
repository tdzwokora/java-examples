package com.tinashe.annotations;

public @interface Exercise {
    int hoursPerDay();
    int startHour() default 6;
}
