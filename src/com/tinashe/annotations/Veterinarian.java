package com.tinashe.annotations;

public class Veterinarian {
    @ZooAnimal(habitat="Infirmary") private Lion sickLion;
    @ZooAnimal(habitat = "Safari") private Lion healthyLion;
    @ZooAnimal(habitat = "Special Enclosure") private Lion blindLion;
}
